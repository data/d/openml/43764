# OpenML dataset: Data-on-COVID-19-(coronavirus)

https://www.openml.org/d/43764

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Coronavirus Country Profiles
We built 207 country profiles which allow you to explore the statistics on the coronavirus pandemic for every country in the world.
In a fast-evolving pandemic it is not a simple matter to identify the countries that are most successful in making progress against it. Excess mortality and the rate of the confirmed deaths is what we focus on in the sections below, but for a fuller assessment a wider perspective is useful. For this purpose we track the impact of the pandemic across our publication and we built country profiles for 207 countries to study the statistics on the coronavirus pandemic for every country in the world in depth.
Each profile includes interactive visualizations, explanations of the presented metrics, and the details on the sources of the data.
Every country profile is updated daily.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43764) of an [OpenML dataset](https://www.openml.org/d/43764). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43764/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43764/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43764/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

